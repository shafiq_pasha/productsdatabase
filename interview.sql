-- MySQL dump 10.13  Distrib 5.7.32, for osx10.12 (x86_64)
--
-- Host: localhost    Database: interview
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` varchar(10) NOT NULL,
  `stock_quantity` int(11) NOT NULL,
  `manufacturer` varchar(30) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'iPhone 13 Pro Max','1200',78,'Apple','2021-12-23 05:14:17','2021-12-23 05:14:17'),(2,'iPad Pro','729',64,'Apple','2021-12-23 05:14:17','2021-12-23 05:14:17'),(3,'Samsung Galaxy S21','599',43,'Samsung','2021-12-23 05:14:17','2021-12-23 05:14:17'),(4,'Galaxy Buds Pro','79',76,'Samsung','2021-12-23 05:14:17','2021-12-23 05:14:17'),(5,'Airpods Pro','250',51,'Apple','2021-12-23 05:14:17','2021-12-23 05:14:17'),(6,'OnePlus 9 Pro 5G ','800',113,'One Plus','2021-12-23 05:14:17','2021-12-23 05:14:17'),(7,'OnePlus Buds Pro','119',50,'One Plus','2021-12-23 05:14:17','2021-12-23 05:14:17'),(8,'Google Pixel 5 5G','799',49,'Google','2021-12-23 05:14:17','2021-12-23 05:14:17'),(9,'HUAWEI P50 Pro','1199',6,'Huawei','2021-12-23 05:14:17','2021-12-23 05:14:17'),(10,'Sony WH-1000XM4 Wireless Headphones','248',8,'Sony','2021-12-23 05:14:17','2021-12-23 05:14:17'),(11,'MacBook Pro 16‑inch (M1 Pro)','2499',0,'Apple','2021-12-23 05:14:17','2021-12-23 05:14:17');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-30 16:27:00
